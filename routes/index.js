var express = require("express");
var router = express.Router();
var { Todos } = require("../models/todos");

/* GET home page. */
router.get("/", async function (req, res, next) {
  const todos = await Todos.find();
  // {description: "doing something", complete: true }
  res.render("index", { title: "Express", todos: todos });
});

module.exports = router;
